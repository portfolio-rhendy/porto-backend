//Define model
module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
            company : String,
            role : String,
            description : String,
            yearStart : Number,
            monthStart : String,
            yearEnd : Number,
            monthEnd : String
        },
    )

    schema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const Work = mongoose.model(
        "work",
        schema
    );

    return Work;
}