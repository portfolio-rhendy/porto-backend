const dbConfig = require("../config/db.config");
const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;
db.educations = require("./education.model.js")(mongoose);
db.volunteers = require("./volunteer.model.js")(mongoose);
db.works = require("./work.model.js")(mongoose);

module.exports = db;