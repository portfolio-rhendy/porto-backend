//Define model
module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
            school : String,
            fieldOfStudy : String,
            yearStart : Number,
            yearEnd : Number
        },
    )

    schema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const Education = mongoose.model(
        "education",
        schema
    );

    return Education;
}