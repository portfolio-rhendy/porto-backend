//Define model
module.exports = mongoose => {
    var schema = mongoose.Schema(
        {
            event : String,
            role : String,
            year : Number
        },
    )

    schema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const Volunteer = mongoose.model(
        "volunteer",
        schema
    );

    return Volunteer;
}