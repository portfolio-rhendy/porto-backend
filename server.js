const express = require("express");
const cors = require("cors");

const app = express();


const CORS_URL = process.env.CORS_URL;
var corsOptions = {
  origin: CORS_URL
};

app.use(cors());

// parse requests of content-type - application/json
app.use(express.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

const db = require("./model");

db.mongoose
  .connect(db.url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    console.log("Connected to database");
  })
  .catch(err => {
    console.log("Failed to connect to database");
    process.exit();
  })

// simple route
app.get("/", (req, res) => {
    res.json({ message: "Welcome to my portfolio website." });
});


require("./route/education.route")(app);
require("./route/volunteer.route")(app);
require("./route/work.route")(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});