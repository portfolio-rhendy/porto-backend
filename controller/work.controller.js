const db = require("../model");
const Work = db.works;

exports.create = (req, res) => {
    // Create an education
    const work = new Work({
        company : req.body.company,
        role : req.body.role,
        description : req.body.description,
        yearStart : req.body.yearStart,
        monthStart : req.body.monthStart,
        yearEnd : req.body.yearEnd,
        monthEnd : req.body.monthEnd
    });
    
    // Save education in the database
    work
        .save()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the works."
        });
    });
};

exports.findAll = (req, res) => {
    Work.find()
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving works."
        });
    });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Work.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Work with id " + id });
            else res.send(data);
        })
        .catch(err => {
        res.status(500).send({ 
            message: "Error retrieving Work with id=" + id });
        });
};

exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
          message: "Data to update can not be empty!"
        });
    }
    
    const id = req.params.id;
    
    Work.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Work with id=${id}. Maybe it was not found!`
                });
            } 
            else res.send({ 
                message: "Work was updated successfully." 
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Work with id=" + id
            });
    });
}