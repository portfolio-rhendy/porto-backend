const db = require("../model");
const Volunteer = db.volunteers;

exports.create = (req, res) => {
    // Create a volunteer
    const volunteer = new Volunteer({
        event : req.body.event,
        role : req.body.role,
        year : req.body.year
    });
    
    // Save volunteer in the database
    volunteer
        .save()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the education."
        });
    });
};

exports.findAll = (req, res) => {
    Volunteer.find()
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving tutorials."
        });
    });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Volunteer.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Volunteer with id " + id });
            else res.send(data);
        })
        .catch(err => {
        res.status(500).send({ 
            message: "Error retrieving Volunteer with id=" + id });
        });
};

exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
          message: "Data to update can not be empty!"
        });
    }
    
    const id = req.params.id;
    
    Volunteer.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Volunteer with id=${id}. Maybe it was not found!`
                });
            } 
            else res.send({ 
                message: "Volunteer was updated successfully." 
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Volunteer with id=" + id
            });
        });
}