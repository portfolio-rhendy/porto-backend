const db = require("../model");
const Education = db.educations;

exports.create = (req, res) => {
    // Create an education
    const education = new Education({
        school : req.body.school,
        fieldOfStudy : req.body.fieldOfStudy,
        yearStart : req.body.yearStart,
        yearEnd : req.body.yearEnd,
    });
    
    // Save education in the database
    education
        .save()
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the education."
        });
    });
};

exports.findAll = (req, res) => {
    Education.find()
    .then(data => {
        res.send(data);
    })
    .catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving tutorials."
        });
    });
};

exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Education.findById(id)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Education with id " + id });
            else res.send(data);
        })
        .catch(err => {
        res.status(500).send({ 
            message: "Error retrieving Educations with id=" + id });
        });
};

exports.update = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
          message: "Data to update can not be empty!"
        });
    }
    
    const id = req.params.id;
    
    Education.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Education with id=${id}. Maybe it was not found!`
                });
            } 
            else res.send({ 
                message: "Education was updated successfully." 
            });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Education with id=" + id
            });
        });
}