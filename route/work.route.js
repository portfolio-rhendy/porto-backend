module.exports = app => {
    const works = require("../controller/work.controller.js");

    var router = require("express").Router();

    router.post("/", works.create);
    router.get("/", works.findAll);
    router.get("/:id", works.findOne);
    
    app.use("/api/work", router);
};