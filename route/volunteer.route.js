module.exports = app => {
    const volunteers = require("../controller/volunteer.controller.js");

    var router = require("express").Router();

    router.post("/", volunteers.create);
    router.get("/", volunteers.findAll);
    router.get("/:id", volunteers.findOne);
    
    app.use("/api/volunteer", router);
};