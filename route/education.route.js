module.exports = app => {
    const educations = require("../controller/education.controller.js");

    var router = require("express").Router();

    router.post("/", educations.create);
    router.get("/", educations.findAll);
    router.get("/:id", educations.findOne);
    
    app.use("/api/education", router);
};